package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Address;
import com.codingraja.domain.Customer;

public class SaveCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("config/hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Address address = new Address("H-10", "MRA Building", "Bangalore", "Karnataka", 560068L);
	
		Customer customer = new Customer("Coding", "RAJA", "info@codingraja.com", 9742900696L, address);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(customer);
		transaction.commit();
		session.close();
		
		System.out.println("Customer Record has been saved successfully!");
	}
}
